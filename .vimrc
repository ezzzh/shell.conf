" отменяем стрeлки как буквы
set nocp

" подсветка синтаксиса
syntax on

" нумерация строк
set number

" подключаем цветовую схему
colorscheme ezzzh

" необходимо для отображения 256 цветов
if &term =~? 'mlterm\|xterm'
  set t_Co=256
endif

" подсветка строки
set cursorline

" подсветка поиска
set hlsearch

" организуем табуляцию
set tabstop=4 shiftwidth=4 softtabstop=4 smarttab smartindent

set fillchars+=vert:│
set listchars=eol:¬,tab:»·,trail:•,extends:>,precedes:<,nbsp:+
set list

" правильный перенос строк с сохранением позиции
set smartindent

set encoding=utf-8
scriptencoding utf-8

highlight clear SignColumn

set signcolumn=yes
set incsearch
set wildmode=longest,list,full
set wildmenu
